#include <vector>
#include <iostream>

using namespace std;

auto num=0;
vector<int> myVector;

int main(){
	//read five integers from user and store in a vector
	for (int index=0; index<5; index++){
		cin>>num;
		myVector.push_back(num);
	}
	//print the values store in the vector
	for (auto theNums:myVector){
		cout<<theNums<<"\t";
	}
}
